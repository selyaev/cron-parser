package me.selyaev.deliveroo.cron;

import org.junit.Test;

import static org.junit.Assert.*;

public class CronParserTest {

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_malformed_expression() {
        new CronParser().parse("* * *");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_missing_command() {
        new CronParser().parse("* * * * *");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_text_value() {
        CronParser.parseSingle(Field.hour, "1pm");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_month() {
        CronParser.parseSingle(Field.month, "January");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_day_of_week() {
        CronParser.parseSingle(Field.dayOfWeek, "Monday");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_text_range() {
        CronParser.parseSingle(Field.month, "DEC-MAR");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_int_range() {
        CronParser.parseSingle(Field.month, "2-0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_hour() {
        CronParser.parseSingle(Field.hour, "59");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_int_value() {
        CronParser.parseSingle(Field.hour, "59");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_on_invalid_range() {
        CronParser.parseSingle(Field.hour, "0-59");
    }

    @Test
    public void should_parse_month() {
        assertEquals(0, CronParser.parseSingle(Field.month, "JAN"));
        assertEquals(11, CronParser.parseSingle(Field.month, "DEC"));
    }

    @Test
    public void should_parse_day_of_week() {
        assertEquals(1, CronParser.parseSingle(Field.dayOfWeek, "SUN"));
        assertEquals(7, CronParser.parseSingle(Field.dayOfWeek, "SAT"));
    }

    @Test
    public void should_parse_int_range() {
        assertArrayEquals(
                new int[]{0, 1, 2, 3, 4, 5},
                CronParser.parseRange(Field.minute, "0-5")
        );

        assertArrayEquals(
                new int[]{14, 15, 16},
                CronParser.parseRange(Field.hour, "14-16")
        );
    }

    @Test
    public void should_parse_text_range() {
        assertArrayEquals(
                new int[]{1, 2, 3, 4},
                CronParser.parseRange(Field.dayOfWeek, "SUN-WED")
        );

        assertArrayEquals(
                new int[]{2, 3, 4},
                CronParser.parseRange(Field.month, "MAR-MAY")
        );
    }

    @Test
    public void should_parse_interval() {
        assertArrayEquals(
                new int[]{0, 4, 8, 12, 16, 20},
                CronParser.parseInterval(Field.hour, "0/4")
        );
        assertArrayEquals(
                new int[]{0, 15, 30, 45},
                CronParser.parseInterval(Field.minute, "*/15")
        );
    }

    @Test
    public void should_parse_simple_int_list() {
        assertArrayEquals(
                new int[]{1, 2, 3},
                CronParser.parseList(Field.minute, "1,2,3")
        );
    }

    @Test
    public void should_parse_simple_text_list() {
        assertArrayEquals(
                new int[]{2, 3},
                CronParser.parseList(Field.dayOfWeek, "MON,TUE")
        );
    }

    @Test
    public void should_parse_complex_text_list() {
        assertArrayEquals(
                new int[]{2, 6, 7},
                CronParser.parseList(Field.dayOfWeek, "MON,FRI-SAT")
        );
    }

    @Test
    public void should_parse_complex_int_list() {
        assertArrayEquals(
                new int[]{0, 14, 15, 16, 23},
                CronParser.parseList(Field.hour, "0,14-16,23")
        );
    }

    @Test
    public void should_support_spaces_in_command() {
        var expression = new CronParser().parse("* * * * * time /usr/bin/find");
        assertEquals("time /usr/bin/find", expression.command);
    }

    @Test
    public void should_parse_sample_input() {
        var expression = new CronParser().parse("*/15 0 1,15 * 1-5 /usr/bin/find");

        assertEquals("/usr/bin/find", expression.command);

        assertField(expression, Field.minute, 0, 15, 30, 45);
        assertField(expression, Field.hour, 0);
        assertField(expression, Field.dayOfMonth, 1, 15);
        assertField(expression, Field.month, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        assertField(expression, Field.dayOfWeek, 1, 2, 3, 4, 5);
    }

    private static void assertField(Schedule schedule, Field field, int... expected) {
        assertArrayEquals(field.name(), expected, schedule.fields.get(field));
    }
}