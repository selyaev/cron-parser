package me.selyaev.deliveroo.cron;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.util.stream.IntStream.rangeClosed;

/**
 * Supports standard cron format with five time fields (minute, hour, day of month, month, and day of week) plus a command. <br/>
 *
 * TODO add support of L, W, C, # expressions
 */
public class CronParser {

    private static List<String> DAYS = Arrays.asList("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");

    private static List<String> MONTHS = Arrays.asList("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");

    Schedule parse(String expression) {
        var arr = expression.split("\\s+");
        if (arr.length <= Field.values().length) {
            throw new IllegalArgumentException("Malformed expression. Expected 5 fields and command, space separated.");
        }

        EnumMap<Field, int[]> fields = new EnumMap<>(Field.class);
        for (int i = 0; i < Field.values().length; i++) {
            String expr = arr[i];
            var field = Field.values()[i];
            fields.put(field, parse(field, expr));
        }

        return new Schedule(getCommand(arr), fields);
    }

    private String getCommand(String[] arr) {
        StringBuilder sb = new StringBuilder();
        for (int i = Field.values().length; i < arr.length; i++) {
            sb.append(arr[i]).append(" ");
        }
        return sb.toString().trim();
    }

    private int[] parse(Field field, String expression) {
        if ("*".equals(expression) || "?".equals(expression)) {
            return field.all();
        }
        if (expression.contains("/")) {
            return parseInterval(field, expression);
        }
        if (expression.contains(",")) {
            return parseList(field, expression);
        }
        if (expression.contains("-")) {
            return parseRange(field, expression);
        }
        return new int[]{parseSingle(field, expression)};
    }

    static int parseSingle(Field field, String value) {
        if (Character.isDigit(value.charAt(0))) {
            var result = parseInt(value);
            if (result > field.max) {
                throw illegalValue(field, value);
            }
            return result;
        }
        switch (field) {
            case month:
                if (!MONTHS.contains(value)) {
                    throw illegalValue(field, value);
                }
                return MONTHS.indexOf(value);
            case dayOfWeek:
                if (!DAYS.contains(value)) {
                    throw illegalValue(field, value);
                }
                return DAYS.indexOf(value) + 1;
        }
        throw illegalValue(field, value);
    }

    static int[] parseInterval(Field field, String expression) {
        var interval = expression.split("/");
        var seed = "*".equals(interval[0]) ? 0 : parseInt(interval[0]);
        var step = parseInt(interval[1]);
        return IntStream.iterate(seed, i -> i + step)
                .takeWhile(i -> i < field.max)
                .toArray();
    }

    static int[] parseList(Field field, String expression) {
        return Stream.of(expression.split(","))
                .flatMapToInt(e -> {
                    if (e.contains("-")) {
                        return IntStream.of(parseRange(field, e));
                    } else {
                        return IntStream.of(parseSingle(field, e));
                    }
                })
                .toArray();
    }

    static int[] parseRange(Field field, String expression) {
        var range = expression.split("-");
        var start = parseSingle(field, range[0]);
        var end = parseSingle(field, range[1]);
        if (end > field.max) {
            throw illegalValue(field, expression);
        }
        if (end < start) {
            throw new IllegalArgumentException(String.format("Illegal range of %s: %s", field, expression));
        }
        return rangeClosed(start, end).toArray();
    }

    private static IllegalArgumentException illegalValue(Field field, String value) {
        return new IllegalArgumentException(String.format("Illegal value of %s: %s", field, value));
    }
}
