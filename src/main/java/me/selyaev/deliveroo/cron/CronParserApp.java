package me.selyaev.deliveroo.cron;

import java.util.stream.IntStream;

import static java.lang.String.join;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;

/**
 * Entry point for {@link CronParser}. <br/>
 * Output is formatted as a table with the field name taking the first {@link #COLUMN_WIDTH} columns
 * and the times as a space-separated list following it. <br/>
 * For example:
 * <pre>
 *   minute        0 15 30 45
 *   hour          0
 *   day of month  1 15
 *   month         1 2 3 4 5 6 7 8 9 10 11 12
 *   day of week   1 2 3 4 5
 *   command       /usr/bin/find
 * </pre>
 *
 * TODO: better error messages, validation, hints, help parameter, man, etc
 */
public class CronParserApp {

    private static final int COLUMN_WIDTH = 14;

    private static String format(Schedule schedule, int padding) {
        return schedule.fields.entrySet().stream()
                .map(e -> format(e.getKey().label, padding) + spaceDelimited(e.getValue()))
                .collect(joining(lineSeparator())) + lineSeparator() + format("command", padding) + schedule.command;
    }

    private static String spaceDelimited(int[] array) {
        return IntStream.of(array).mapToObj(String::valueOf).collect(joining(" "));
    }

    private static String format(String s, int padding) {
        return s + " ".repeat(padding - s.length());
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Missing argument.");
            System.err.println("Valid cron expression expected, for example: CronParser */15 0 1,15 * 1-5 /usr/bin/find");
            System.exit(1);
        }

        try {
            var expression = new CronParser().parse(join(" ", args));
            System.out.println(format(expression, COLUMN_WIDTH));
        } catch (IllegalArgumentException e) {
            System.err.println("Could not generate schedule.");
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}
