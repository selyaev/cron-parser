package me.selyaev.deliveroo.cron;

import static java.util.stream.IntStream.rangeClosed;

/**
 * Part of standard Cron expression with label and limits.
 */
public enum Field {
    minute(0, 59),
    hour(0, 23),
    dayOfMonth("day of month", 1, 31),
    month(1, 12),
    dayOfWeek("day of week", 1, 7);

    public final String label;
    public final int min;
    public final int max;

    Field(int min, int max) {
        this(null, min, max);
    }

    Field(String label, int min, int max) {
        this.label = label != null ? label : name();
        this.min = min;
        this.max = max;
    }

    public int[] all() {
        return rangeClosed(min, max).toArray();
    }
}
