package me.selyaev.deliveroo.cron;

import java.util.EnumMap;
import java.util.Objects;

/**
 * Structured representation of Cron expression.
 */
public class Schedule {

    final String command;

    final EnumMap<Field, int[]> fields;

    Schedule(String command, EnumMap<Field, int[]> fields) {
        this.fields = fields;
        this.command = command;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule that = (Schedule) o;
        return Objects.equals(command, that.command) &&
                Objects.equals(fields, that.fields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(command, fields);
    }

    @Override
    public String toString() {
        return "Expression{" +
                "command='" + command + '\'' +
                ", fields=" + fields +
                '}';
    }
}
