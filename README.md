# Cron Expression Parser

Command line application which parses a cron string and expands each field to show the times at which it will run.

Supports standard cron format with five time fields (minute, hour, day of month, month, and day of week) plus a command.

The cron string is passed on the command line as a single argument on a single line. For example:

    java me.selyaev.deliveroo.cron.CronParserApp */15 0 1,15 * 1-5 /usr/bin/find
    
The output is formatted as a table with the field name taking the first 14 columns and the times as a space-separated list following it.
For example, the following input argument:

    */15 0 1,15 * 1-5 /usr/bin/find
    
yields the following output:

    minute        0 15 30 45
    hour          0
    day of month  1 15
    month         1 2 3 4 5 6 7 8 9 10 11 12
    day of week   1 2 3 4 5
    command       /usr/bin/find
    
## Developers

Parsing logic and main stating point is in `me.selyaev.deliveroo.cron.CronParser`.

Tests are in `me.selyaev.deliveroo.cron.CronParserTest`.

Run project using gradle: 

    gradle run --args="*/15 0 1,15 * 1-5 /usr/bin/find"
    
Run tests using gradle:
    
    gradle test